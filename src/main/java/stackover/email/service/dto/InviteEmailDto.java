package stackover.email.service.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import stackover.email.service.enums.MailLang;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Schema (description = "DTO для отправления приглашения по email")
public record InviteEmailDto(
        @Schema(description = "Email пользователя")
        @NotBlank @Email String email,
        @Schema(description = "Язык email")
        @NotNull MailLang lang) {
}
