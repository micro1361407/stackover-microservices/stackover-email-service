package stackover.email.service.rest;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stackover.email.service.dto.InviteEmailDto;
import stackover.email.service.dto.NewPasswordEmailDto;
import stackover.email.service.dto.RecoveryEmailDto;
import stackover.email.service.service.MailService;

@Tag(name = "MailRestController", description = "Контроллер для отправки писем")
@Slf4j
@RestController
@RequestMapping("/api/inner/mail")
@Validated
public class MailRestController {
    private final MailService mailService;

    @Autowired
    public MailRestController(MailService mailService) {
        this.mailService = mailService;
    }

    @Operation(summary = "Отправка приглашения пользователю, содержащее ссылку с подтверждением email")
    @PostMapping("/invite")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Приглашение успешно отправлено"),
            @ApiResponse(responseCode = "400", description = "Неверный запрос или данные")
    })
    public ResponseEntity<String> sendEmailAboutRegistration(@RequestBody @Validated
                                                                 @Parameter(description = "InviteEmailDto") InviteEmailDto inviteEmailDto) {
        try {
            mailService.sendInviteEmail(inviteEmailDto.email(), inviteEmailDto.lang());
            log.info("Invitation email sent successfully");
            return ResponseEntity.ok("Invitation email sent successfully.");
        } catch (Exception e) {
            log.error(String.format("Error sending email: %s", e));
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error sending email: " + e.getMessage());
        }
    }

    @Operation(summary = "Отправка сообщения для восстановления пароля")
    @PostMapping("/recovery")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ссылка для восстановления пароля успешно отправлена"),
            @ApiResponse(responseCode = "400", description = "Неверный запрос или данные")
    })
    public ResponseEntity<String> sendRecoveryLink(@RequestBody @Validated
                                                       @Parameter(description = "RecoveryEmailDto") RecoveryEmailDto recoveryEmailDto) {
        try {
            mailService.sendRecoveryEmail(recoveryEmailDto.email(), recoveryEmailDto.lang());
            log.info("Recovery email sent successfully.");
            return ResponseEntity.ok("Recovery email sent successfully.");
        } catch (Exception e) {
            log.error(String.format("Error sending email: %s", e));
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error sending email: " + e.getMessage());
        }
    }

    @Operation(summary = "Отправка сообщения о смене пароля")
    @PostMapping("/new-password")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Сообщение о смене пароля успешно отправлено"),
            @ApiResponse(responseCode = "400", description = "Неверный запрос или данные")
    })
    public ResponseEntity<String> sendNewPasswordByEmail(@RequestBody @Validated
                                                             @Parameter(description = " NewPasswordEmailDto") NewPasswordEmailDto newPasswordEmailDto) {
        try {
            mailService.sendNewPasswordByEmail(newPasswordEmailDto.email(), newPasswordEmailDto.lang(), newPasswordEmailDto.password());
            log.info("New password email sent successfully.");
            return ResponseEntity.ok("New password email sent successfully.");
        } catch (Exception e) {
            log.error(String.format("Error sending email: %s", e));
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error sending email: " + e.getMessage());
        }
    }
}
