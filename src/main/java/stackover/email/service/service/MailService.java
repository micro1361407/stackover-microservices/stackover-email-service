package stackover.email.service.service;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import stackover.email.service.enums.MailLang;
import stackover.email.service.enums.MailType;
import stackover.email.service.factory.MailTemplateFactory;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@Service
public class MailService {
    //TODO реализовать получение инфо о user через auth service. JavaMailSender убрать.
    private final RestTemplate restTemplate;
    private final MailTemplateFactory mailTemplateFactory;
    private final JavaMailSender mailSender;
    private final String authServiceUrl = "http://localhost:8180/api/auth";

    @Autowired
    public MailService(RestTemplate restTemplate, MailTemplateFactory mailTemplateFactory, JavaMailSender mailSender) {
        this.restTemplate = restTemplate;
        this.mailTemplateFactory = mailTemplateFactory;
        this.mailSender = mailSender;
    }

    public void sendInviteEmail(String email, MailLang lang) throws IOException, TemplateException, MessagingException {
        Map<String, Object> model = new HashMap<>();
        model.put("link", "ССЫЛКА");
        sendEmail(email, lang, MailType.INVITE, "Email Confirmation", model);
    }

    public void sendRecoveryEmail(String email, MailLang lang) throws IOException, TemplateException, MessagingException {
        Map<String, Object> model = new HashMap<>();
        model.put("link", "ССЫЛКА");
        sendEmail(email, lang, MailType.RECOVERY_PASSWORD, "Password Recovery", model);
    }

    public void sendNewPasswordByEmail(String email, MailLang lang, String password) throws IOException, TemplateException, MessagingException {
        Map<String, Object> model = new HashMap<>();
        model.put("email", email);
        model.put("password", password);
        sendEmail(email, lang, MailType.NEW_PASSWORD, "New Password", model);
    }


    private void sendEmail(String email, MailLang lang, MailType mailType, String subject, Map<String, Object> model) throws IOException, TemplateException, MessagingException {
        Template template = mailTemplateFactory.getTemplate(mailType, lang);

        StringWriter stringWriter = new StringWriter();
        template.process(model, stringWriter);
        String emailContent = stringWriter.getBuffer().toString();

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(email);
        helper.setSubject(subject);
        helper.setText(emailContent, true);

        mailSender.send(message);
    }

}
