<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title></title>
</head>
<body>
<p>
    <span>Пароль успешно восстановлен</span>
</p>

<p>
    <span>Ваш логин:</span>
    <span>${email}</span>
</p>
<p>
    <span>Ваш пароль:</span>
    <span>${password}</span>
</p>
<p>
    <span>Это ваши новые данные. Изменить пароль вы можете в настройках профиля.</span>
</p>
</body>
</html>