<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
</head>
<body>
<p>
    <span>Your password recovery link</span>
</p>
<p>
    <span>To restore your password, follow the</span>
    <a href="${link}">link</a>
</p>
</body>
</html>